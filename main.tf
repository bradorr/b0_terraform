###setting provider as aws
provider "aws" {}

###contains vpc, subnets, internet gateway, and NAT 
resource "aws_vpc" "default" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "bakery-aws-vpc"
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

###public subnets and their routing reference 
resource "aws_subnet" "us-west-1a-public" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "${var.public_subnet1_cidr}"
  availability_zone       = "us-west-1a"
  map_public_ip_on_launch = true

  tags {
    Name = "Public Subnet1"
  }
}

resource "aws_subnet" "us-west-1b-public" {
  vpc_id = "${aws_vpc.default.id}"

  cidr_block              = "${var.public_subnet2_cidr}"
  availability_zone       = "us-west-1b"
  map_public_ip_on_launch = true

  tags {
    Name = "Public Subnet2"
  }
}

###Private subnets and their routing references 

resource "aws_subnet" "us-west-1a-private" {
  vpc_id = "${aws_vpc.default.id}"

  cidr_block        = "${var.private_subnet1_cidr}"
  availability_zone = "us-west-1a"

  tags {
    Name = "Private Subnet1"
  }
}

resource "aws_subnet" "us-west-1b-private" {
  vpc_id = "${aws_vpc.default.id}"

  cidr_block        = "${var.private_subnet2_cidr}"
  availability_zone = "us-west-1b"

  tags {
    Name = "Private Subnet2"
  }
}

resource "aws_eip" "nateip1" {
  vpc = true
}

resource "aws_eip" "nateip2" {
  vpc = true
}

resource "aws_nat_gateway" "private1" {
  allocation_id = "${aws_eip.nateip1.id}"
  subnet_id     = "${aws_subnet.us-west-1a-public.id}"
}

resource "aws_nat_gateway" "private2" {
  allocation_id = "${aws_eip.nateip2.id}"
  subnet_id     = "${aws_subnet.us-west-1b-public.id}"
}
