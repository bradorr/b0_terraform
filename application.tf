resource "aws_launch_configuration" "app" {
  name            = "app_launch_config"
  image_id        = "ami-0f31331925d4c1290"
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.app.id}"]
}

resource "aws_autoscaling_group" "app" {
  name                      = "app_scaling_group"
  launch_configuration      = "${aws_launch_configuration.app.name}"
  min_size                  = 2
  max_size                  = 2
  vpc_zone_identifier       = ["${aws_subnet.us-west-1a-private.id}", "${aws_subnet.us-west-1b-private.id}"]
  health_check_grace_period = 30000
  load_balancers            = ["${aws_elb.app.name}"]
}

resource "aws_security_group" "app" {
  name        = "app_security_group"
  description = "Allows outbound access to RDS and Inbound from load balancer"
  vpc_id      = "${aws_vpc.default.id}"
}

resource "aws_security_group_rule" "outbound_rds" {
  security_group_id        = "${aws_security_group.app.id}"
  type                     = "egress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.db.id}"
}

resource "aws_security_group_rule" "inbound_lb" {
  security_group_id        = "${aws_security_group.app.id}"
  type                     = "ingress"
  from_port                = 9000
  to_port                  = 9000
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.app_lb.id}"
}

resource "aws_elb" "app" {
  name            = "application-lb"
  security_groups = ["${aws_security_group.app_lb.id}"]
  subnets         = ["${aws_subnet.us-west-1a-private.id}", "${aws_subnet.us-west-1b-private.id}"]
  internal        = true

  listener {
    instance_port     = 9000
    instance_protocol = "TCP"
    lb_port           = 9000
    lb_protocol       = "TCP"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:9000"
    interval            = 15
  }
}

resource "aws_security_group" "app_lb" {
  name        = "app_load_balancer_sg"
  description = "Allows inbound FCG calls from web tier, and out to app servers"
  vpc_id      = "${aws_vpc.default.id}"
}

resource "aws_security_group_rule" "inbound_fcgi" {
  security_group_id        = "${aws_security_group.app_lb.id}"
  type                     = "ingress"
  from_port                = 9000
  to_port                  = 9000
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "outbound_fcgi" {
  security_group_id        = "${aws_security_group.app_lb.id}"
  type                     = "egress"
  from_port                = 9000
  to_port                  = 9000
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.app.id}"
}
