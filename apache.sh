#!/bin/bash -e
sudo apt-get update -y
sudo apt-get install apache2 -y
sudo apache2ctl configtest
sudo ufw allow in "Apache Full"
sudo chmod ugo+rwx -R  /var/www/html/
sudo echo "<html><body><h1>Stuart's Web Server 3</h1></body></html>" > /var/www/html/index.html
