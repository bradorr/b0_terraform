resource "aws_db_instance" "main" {
  name                   = "Bo_database"
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "mysql"
  instance_class         = "db.t2.micro"
  multi_az               = true
  skip_final_snapshot    = true
  username               = "genericusername"
  password               = "genericpassword"
  db_subnet_group_name   = "${aws_db_subnet_group.main.id}"
  vpc_security_group_ids = ["${aws_security_group.db.id}"]
}

resource "aws_db_subnet_group" "main" {
  name       = "database-subnet-group"
  subnet_ids = ["${aws_subnet.us-west-1a-private.id}", "${aws_subnet.us-west-1b-private.id}"]
}

resource "aws_security_group" "db" {
  name        = "database-sg"
  description = "Allows traffic from App servers to rds"
  vpc_id      = "${aws_vpc.default.id}"
}

resource "aws_security_group_rule" "inbound_app" {
  security_group_id        = "${aws_security_group.db.id}"
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.app.id}"
}
