resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route" "web_internet" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

resource "aws_route_table_association" "pub_sub1" {
  subnet_id      = "${aws_subnet.us-west-1a-public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "pub_sub2" {
  subnet_id      = "${aws_subnet.us-west-1b-public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table" "private1" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route" "privare_nat1" {
  route_table_id         = "${aws_route_table.private1.id}"
  nat_gateway_id         = "${aws_nat_gateway.private1.id}"
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "pri_sub1" {
  subnet_id      = "${aws_subnet.us-west-1a-private.id}"
  route_table_id = "${aws_route_table.private1.id}"
}

resource "aws_route_table" "private2" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route" "privare_nat2" {
  route_table_id         = "${aws_route_table.private2.id}"
  nat_gateway_id         = "${aws_nat_gateway.private2.id}"
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "pri_sub2" {
  subnet_id      = "${aws_subnet.us-west-1a-private.id}"
  route_table_id = "${aws_route_table.private1.id}"
}
