resource "aws_security_group" "web" {
  name        = "vpc_web"
  description = "Allow incoming HTTP connections."
  vpc_id      = "${aws_vpc.default.id}"
}

resource "aws_security_group_rule" "inbound_elb" {
  security_group_id        = "${aws_security_group.web.id}"
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.elb.id}"
}

resource "aws_launch_configuration" "webserver" {
  image_id        = "ami-0f31331925d4c1290"
  instance_type   = "t1.micro"
  security_groups = ["${aws_security_group.web.id}"]
}

resource "aws_autoscaling_group" "web" {
  name                      = "web-scale-group"
  launch_configuration      = "${aws_launch_configuration.webserver.name}"
  min_size                  = 2
  max_size                  = 2
  health_check_grace_period = 30000
  health_check_type         = "ELB"
  vpc_zone_identifier       = ["${aws_subnet.us-west-1a-public.id}", "${aws_subnet.us-west-1b-public.id}"]
  load_balancers            = ["${aws_elb.web.id}"]
}
