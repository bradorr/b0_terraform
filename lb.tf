resource "aws_security_group" "elb" {
  name        = "elb-security-group"
  description = "allow http to and from elb on port 80"
  vpc_id      = "${aws_vpc.default.id}"
}

resource "aws_security_group_rule" "outbound_http" {
  security_group_id        = "${aws_security_group.elb.id}"
  type                     = "egress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.web.id}"
}

resource "aws_elb" "web" {
  name                      = "web-facing-elb"
  security_groups           = ["${aws_security_group.elb.id}"]
  subnets                   = ["${aws_subnet.us-west-1a-public.id}", "${aws_subnet.us-west-1b-public.id}"]
  cross_zone_load_balancing = true
  idle_timeout              = 400

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 15
  }
}
